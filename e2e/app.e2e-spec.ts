import { ProblemsWorthSolvingPage } from './app.po';

describe('problems-worth-solving App', function() {
  let page: ProblemsWorthSolvingPage;

  beforeEach(() => {
    page = new ProblemsWorthSolvingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
