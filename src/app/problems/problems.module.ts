import { NgModule } from '@angular/core';

import { CoreModule } from 'app/core/core.module';

import { ProblemsContainerComponent } from './problems-container.component';
import { ProblemComponent } from './problem.component';
import { FoodComponent } from './food.component';


@NgModule({
  imports: [
    CoreModule
  ],
  declarations: [ ProblemsContainerComponent, FoodComponent, ProblemComponent ]
})
export class ProblemsModule { }
