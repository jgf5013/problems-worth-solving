import { Component, OnInit } from '@angular/core';

import * as Highcharts from 'highcharts';

import chartOptions from './chartOptions.json';
import data from './Food Supply - Crops Primary Equivalent.json';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss']
})
export class FoodComponent implements OnInit {

  chartOptions: any;
  data: any[];
  constructor() {
    console.log('data: ', data);
    this.data = data;
    this.chartOptions = chartOptions;
    this.chartOptions.series.push({
      data: this.data.map((d) => {
        return [d.Year, d.Value];
      })
    });
  }

  ngOnInit() {
      Highcharts.chart('visualization-1', this.chartOptions);
      Highcharts.chart('visualization-2', this.chartOptions);
  }

}
