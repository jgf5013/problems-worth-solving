import { Action } from '@ngrx/store';

export enum LayoutActionTypes {
  OpenSidenav = '[Layout] Open Sidenav',
  CloseSidenav = '[Layout] Close Sidenav',
  AppLoaded = '[Layout] App Loaded'
}

export class OpenSidenav implements Action {
  readonly type = LayoutActionTypes.OpenSidenav;
}

export class CloseSidenav implements Action {
  readonly type = LayoutActionTypes.CloseSidenav;
}

export class AppLoaded implements Action {
  readonly type = LayoutActionTypes.AppLoaded;
}

export type LayoutActionsUnion = OpenSidenav | CloseSidenav | AppLoaded;