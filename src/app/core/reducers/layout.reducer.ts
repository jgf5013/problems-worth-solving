import {
  LayoutActionTypes,
  LayoutActionsUnion,
} from '../actions/layout.actions';

export interface State {
  showSidenav: boolean;
  appLoaded: boolean;
}

const initialState: State = {
  showSidenav: false,
  appLoaded: false
};

export function reducer(
  state: State = initialState,
  action: LayoutActionsUnion
): State {
  switch (action.type) {
    case LayoutActionTypes.CloseSidenav:
      return {
        ...state,
        showSidenav: false
      };

    case LayoutActionTypes.OpenSidenav:
      return {
        ...state,
        showSidenav: true,
      };

    case LayoutActionTypes.AppLoaded:
      return {
        ...state,
        appLoaded: true,
      };

    default:
      return state;
  }
}

export const getShowSidenav = (state: State) => state.showSidenav;