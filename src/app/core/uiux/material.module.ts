import { NgModule } from '@angular/core';
import { MatButtonModule, MatBadgeModule, MatIconModule, MatCardModule, MatToolbarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  imports: [ MatButtonModule, BrowserAnimationsModule, MatIconModule, MatCardModule, MatToolbarModule ],
  declarations: [],
  exports: [ MatButtonModule, BrowserAnimationsModule, MatIconModule, MatCardModule, MatToolbarModule ],
})
export class MaterialModule {
}