import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './uiux/material.module';


@NgModule({
  imports: [CommonModule, RouterModule, MaterialModule],
  declarations: [],
  exports: [CommonModule, RouterModule, MaterialModule],
})
export class CoreModule {
  static forRoot() {
    return {
      ngModule: CoreModule,
      providers: [],
    };
  }
}