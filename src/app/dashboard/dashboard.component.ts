import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';



import quotes from './quotes.json';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public quote: any = quotes[2];

  constructor(private router: Router){
    console.log('quote: ', this.quote);
  }

  ngOnInit() {
  }

  goto(route: string) {
    this.router.navigate([route]);
  }

}
