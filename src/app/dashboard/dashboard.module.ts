import { NgModule } from '@angular/core';

import { CoreModule } from 'app/core/core.module';

import { DashboardComponent } from './dashboard.component';

@NgModule({
  imports: [
    CoreModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
