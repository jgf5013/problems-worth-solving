import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

import { DashboardComponent } from 'app/dashboard/dashboard.component';
import { FoodComponent } from 'app/problems/food.component';
import { PageNotFoundComponent } from 'app/page-not-found/page-not-found.component';
 
const appRoutes: Routes = [
  { path: '', component: DashboardComponent, data: { } },
  { path: 'food', component: FoodComponent, data: { title: 'food' } },
  { path: '**', component: PageNotFoundComponent, data: { title: 'page not found' } }
];
 
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { enableTracing: false })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}