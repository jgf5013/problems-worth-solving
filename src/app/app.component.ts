import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, Event, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { map, filter, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public currentUrl: string;
  public title: string;
  public showBackButton: boolean;
  public isInitialized: boolean = false;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private titleService: Title) {
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
         while (route.firstChild) {
           route = route.firstChild;
         };

         return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data)
    ).subscribe((event) => {
      let title = event.title ?
        `${event['title'][0].toUpperCase() + event['title'].slice(1)}   |   Problems Worth Solving` :
        'Problems Worth Solving';
      this.titleService.setTitle(title);
      this.title = title.toUpperCase();
      
      this.showBackButton = event.title; // this should really check to see if url === '/'
    });
  }


  ngOnInit(): void {
    this.isInitialized = true;
  }

}
